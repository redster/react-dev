import React from 'react';
import PropTypes from 'prop-types';

import './Card.css'

const Card = ({property}) => {
    const {index, picture} = property;
    return (
        <div id={`card-${index}`} className="card">
            <img src={picture} />

        </div>
    )
}

Card.propTypes = {
    property: PropTypes.object.isRequired
}

export default Card;