import React from 'react';
import UseStateHook from "./components/UseStateHook";
import UseEffectHook from "./components/UseEffectHook";

const App = () => {
    return (
        <>
            <UseStateHook/>
            <UseEffectHook/>
            
        </>   
    );    
};

export default App;

