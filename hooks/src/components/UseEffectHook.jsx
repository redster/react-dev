import React, { useState, useEffect }  from 'react';


export default function useStateHook1() {
 
    const [scrollDepth, setscrollDepth] = useState(0);

    function determinateUserScrollDepth() {
        const scrolled = document.documentElement.scrollTop || document.body.scrollTop;
        setscrollDepth(scrolled);
    }
    
    useEffect(() => {
        window.addEventListener('scroll', determinateUserScrollDepth);

        return function() {
            window.removeEventListener('scroll', determinateUserScrollDepth);
        }
    }) 

    return (
        <div>
                <p>You've scrolled this far: {scrollDepth}</p>

                <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Ea laborum labore distinctio unde possimus quaerat, optio excepturi ullam facere molestias repudiandae, nostrum inventore ad nobis sint dolor. Placeat, animi soluta.
                Qui molestiae, dolor corrupti fugit adipisci amet quos cupiditate voluptate nisi eum debitis! Atque quisquam veritatis quasi blanditiis qui repellendus labore autem dolorum nihil, corporis tenetur hic dolorem accusamus quo.
                Blanditiis at minima vitae expedita deleniti omnis dolorum quo minus temporibus, debitis eius ea cum, mollitia nam ab est, architecto iusto iste quod atque voluptas modi quia. Saepe, quaerat repellat?
                Quas, repellat dolorum quidem veritatis quasi qui pariatur nulla numquam dignissimos nisi neque, labore explicabo distinctio possimus ipsum reiciendis quo sequi adipisci veniam quisquam deleniti voluptatum. Blanditiis vel qui enim.
                Fugit, facere aspernatur doloremque saepe architecto quisquam tempora ratione reiciendis vel, ut molestiae iusto magni, voluptatem itaque praesentium. Eius sit magni fugit nisi quasi pariatur quas nemo cupiditate suscipit rerum.
                Aspernatur veritatis veniam mollitia facere officia, debitis inventore a nemo, optio omnis tenetur. Eveniet dicta, ad rerum neque illo inventore mollitia excepturi, quaerat nulla blanditiis fugit, ab in consectetur aspernatur?
                Repellendus, possimus suscipit minima, atque velit error repellat aliquid facere illum ipsam aut cupiditate explicabo id illo cumque nam, obcaecati natus corrupti reprehenderit doloribus ducimus deserunt sunt dolores. Nemo, modi.
                Ipsa, odit ex? Praesentium excepturi provident tempora culpa distinctio alias consequatur expedita, enim reprehenderit ipsa assumenda quod cupiditate quidem ullam quos voluptas recusandae necessitatibus. Quidem optio soluta sit nesciunt in.
                Reiciendis animi doloremque quisquam sint nemo. Porro deserunt unde, eius, ut autem quaerat explicabo laudantium qui nisi assumenda illo eveniet, cumque ipsa quia dolore? Possimus officiis esse sequi iste dolor.
                Quis cupiditate, inventore repellendus veniam nesciunt sed? Eos inventore laboriosam laudantium non exercitationem veniam porro sint neque repellat doloribus cupiditate quam nobis, quo, voluptate cumque cum vel. Est, consequatur expedita.
                Ab consequuntur alias quae magni sed adipisci iusto doloremque iste. Eos impedit minus nisi sunt aperiam quam repellat a nulla officia? Repudiandae, aperiam rerum blanditiis tempora similique officiis ad nihil!
                Rerum sed iure est! Adipisci hic voluptates possimus excepturi consequatur cumque, a commodi labore assumenda repudiandae id cum, esse dolore aspernatur dignissimos delectus eveniet ducimus repellat soluta voluptate ipsam eos!
                Dolor corporis veniam cupiditate odit. Sequi vero repellendus cumque neque, nisi autem voluptatibus dolores eligendi sint sapiente corporis animi illum! Quidem maiores nobis, culpa nemo tempore harum nisi iure facere.
                Dolorem maiores velit non ad nihil, quas animi amet fuga repellendus qui nostrum nisi itaque ea veritatis quod error quos maxime laudantium omnis? Repudiandae vitae dolorum sint minima perferendis optio.
                Temporibus aut error quos repellat saepe atque vitae! Incidunt, voluptas et velit doloribus rerum eveniet, quia sapiente nostrum quasi a possimus non reprehenderit omnis maiores fugit facilis dolor provident debitis.
                Et amet aut eaque necessitatibus dolor commodi praesentium nostrum a. Reprehenderit veritatis eaque sequi maiores velit omnis qui voluptatem illo quas in. Iure quibusdam sed reprehenderit illum? Est, reiciendis officia.
                Iure veniam, quaerat corporis quis velit repudiandae unde porro ea a voluptate molestias. Adipisci unde, vitae doloremque ipsum sunt ipsa culpa architecto numquam, tempore nam, eaque vel consequatur a pariatur.
                Rem fugiat omnis nihil soluta molestias totam sint facilis quidem amet perspiciatis corrupti id pariatur ipsum aspernatur esse, sequi quo repudiandae aliquid harum eaque dolores sit. Rerum eveniet labore quam!
                Facere atque sunt praesentium asperiores blanditiis rerum voluptate, magni repudiandae quam architecto alias reprehenderit eaque est natus quisquam consequuntur quo impedit? Modi eos ut laudantium vitae quod explicabo illum consequuntur?
                Aliquam, est quaerat facilis unde quibusdam esse sequi aut? Totam similique dolorem enim distinctio reiciendis laboriosam laborum sint odio nisi illo cum deserunt consectetur vitae numquam, cupiditate, perferendis culpa dolorum?
                Labore, tempore quam. Hic ullam nostrum possimus quia corporis doloremque obcaecati delectus. Maxime ipsam numquam, omnis asperiores ullam, ducimus quia harum deserunt nam earum minus, culpa recusandae reprehenderit. Modi, placeat.
                Odit aspernatur cupiditate optio libero eligendi tempore quo eum, officia esse? Porro et in nulla eius autem quasi quibusdam ex inventore commodi, quo molestias alias odio assumenda at est. Beatae.
                Magnam asperiores reprehenderit provident architecto reiciendis voluptatibus inventore excepturi, maxime quisquam, qui aperiam molestiae alias sapiente expedita repellat iste quidem veritatis? Expedita rem voluptatem eaque corporis obcaecati earum nobis mollitia!
                Explicabo, exercitationem ab expedita dolorem tempora iusto voluptatem numquam ducimus provident rem esse unde facilis deserunt impedit aperiam at sit delectus cupiditate vitae omnis nisi? Adipisci minus illum aut tempore.
                Tempora molestiae, quas officia at ad sint eos fugit! Sapiente, eos. Sed unde reprehenderit laboriosam ipsam ad excepturi magni nulla, dolores explicabo alias est dolorum sapiente veniam numquam, consequatur voluptatum.
                Dolorum iste itaque perferendis labore debitis ipsa dicta. Commodi nisi distinctio labore aliquid quaerat quia soluta maxime minus dolorum, quam aspernatur alias, sunt asperiores at, nemo aut tenetur officiis? Sapiente!
                Laudantium vero amet, id vel eius natus exercitationem odit illo mollitia maiores deserunt sunt ex praesentium tenetur temporibus cumque dolorum, labore iusto voluptatem omnis esse voluptatibus necessitatibus non! Qui, illo.
                Eius quae perspiciatis eaque expedita debitis aliquid esse ea officia labore, voluptates officiis assumenda beatae cumque, modi dolores maxime accusantium dolorem ipsa sunt, qui repellendus saepe temporibus iure placeat? Sint!
                Officia beatae quae ab laboriosam nam aliquid dignissimos repellat velit eius reprehenderit laborum asperiores omnis, deserunt doloribus suscipit tempora sapiente voluptas eos repudiandae excepturi voluptate. Odio saepe voluptatum inventore provident.
                Aliquam inventore quidem aliquid quos, dolorem similique tenetur impedit facere magni iusto modi, velit earum. Voluptate pariatur eius, quasi alias placeat ipsum, earum quis nesciunt optio quo tempora itaque voluptatibus.
                Ipsum tempora quas quam aliquid adipisci nam facilis praesentium error molestias rerum fugiat, ab sit earum dolores omnis soluta fuga at reprehenderit! Beatae reprehenderit perspiciatis obcaecati! Et quidem vel officia.
                Ea sequi nulla iste eius quasi impedit assumenda eveniet recusandae eos explicabo quidem quis nemo libero itaque aperiam neque laborum voluptas ipsam, aut exercitationem dicta. Nulla quae dolor eius beatae!
                Ratione deserunt ut neque aut officia fugit voluptatem velit possimus necessitatibus blanditiis autem quos incidunt porro deleniti delectus, consequatur nostrum maiores asperiores ipsam? Odio quisquam pariatur quas enim recusandae exercitationem?
                Iste aut animi, quos nisi quod laboriosam bla inventore repellendus veniam nesciunt sed? Eos inventore laboriosam laudantium non exercitationem veniam porro sint neque repellat doloribus cupiditate quam nobis, quo, voluptate cumque cum vel. Est, consequatur expedita.
                Ab consequuntur alias quae magni sed adipisci iusto doloremque iste. Eos impedit minus nisi sunt aperiam quam repellat a nulla officia? Repudiandae, aperiam rerum blanditiis tempora similique officiis ad nihil!
                Rerum sed iure est! Adipisci hic voluptates possimus excepturi consequatur cumque, a commodi labore assumenda repudiandae id cum, esse dolore aspernatur dignissimos delectus eveniet ducimus repellat soluta voluptate ipsam eos!
                Dolor corporis veniam cupiditate odit. Sequi vero repellendus cumque neque, nisi autem voluptatibus dolores eligendi sint sapiente corporis animi illum! Quidem maiores nobis, culpa nemo tempore harum nisi iure facere.
                Dolorem maiores velit non ad nihil, quas animi amet fuga repellendus qui nostrum nisi itaque ea veritatis quod error quos maxime laudantium omnis? Repudiandae vitae dolorum sint minima perferendis optio.
                Temporibus aut error quos repellat saepe atque vitae! Incidunt, voluptas et velit doloribus rerum eveniet, quia sapiente nostrum quasi a possimus non reprehenderit omnis maiores fugit facilis dolor provident debitis.
                Et amet aut eaque necessitatibus dolor commodi praesentium nostrum a. Reprehenderit veritatis eaque sequi maiores velit omnis qui voluptatem illo quas in. Iure quibusdam sed reprehenderit illum? Est, reiciendis officia.
                Iure veniam, quaerat corporis quis velit repudiandae unde porro ea a voluptate molestias. Adipisci unde, vitae doloremque ipsum sunt ipsa culpa architecto numquam, tempore nam, eaque vel consequatur a pariatur.
                Rem fugiat omnis nihil soluta molestias totam sint facilis quidem amet perspiciatis corrupti id pariatur ipsum aspernatur esse, sequi quo repudiandae aliquid harum eaque dolores sit. Rerum eveniet labore quam!
                Facere atque sunt praesentium asperiores blanditiis rerum voluptate, magni repudiandae quam architecto alias reprehenderit eaque est natus quisquam consequuntur quo impedit? Modi eos ut laudantium vitae quod explicabo illum consequuntur?
                Aliquam, est quaerat facilis unde quibusdam esse sequi aut? Totam similique dolorem enim distinctio reiciendis laboriosam laborum sint odio nisi illo cum deserunt consectetur vitae numquam, cupiditate, perferendis culpa dolorum?
                Labore, tempore quam. Hic ullam nostrum possimus quia corporis doloremque obcaecati delectus. Maxime ipsam numquam, omnis asperiores ullam, ducimus quia harum deserunt nam earum minus, culpa recusandae reprehenderit. Modi, placeat.
                Odit aspernatur cupiditate optio libero eligendi tempore quo eum, officia esse? Porro et in nulla eius autem quasi quibusdam ex inventore commodi, quo molestias alias odio assumenda at est. Beatae.
                Magnam asperiores reprehenderit provident architecto reiciendis voluptatibus inventore excepturi, maxime quisquam, qui aperiam molestiae alias sapiente expedita repellat iste quidem veritatis? Expedita rem voluptatem eaque corporis obcaecati earum nobis mollitia!
                Explicabo, exercitationem ab expedita dolorem tempora iusto voluptatem numquam ducimus provident rem esse unde facilis deserunt impedit aperiam at sit delectus cupiditate vitae omnis nisi? Adipisci minus illum aut tempore.
                Tempora molestiae, quas officia at ad sint eos fugit! Sapiente, eos. Sed unde reprehenderit laboriosam ipsam ad excepturi magni nulla, dolores explicabo alias est dolorum sapiente veniam numquam, consequatur voluptatum.
                Dolorum iste itaque perferendis labore debitis ipsa dicta. Commodi nisi distinctio labore aliquid quaerat quia soluta maxime minus dolorum, quam aspernatur alias, sunt asperiores at, nemo aut tenetur officiis? Sapiente!
                Laudantium vero amet, id vel eius natus exercitationem odit illo mollitia maiores deserunt sunt ex praesentium tenetur temporibus cumque dolorum, labore iusto voluptatem omnis esse voluptatibus necessitatibus non! Qui, illo.
                Eius quae perspiciatis eaque expedita debitis aliquid esse ea officia labore, voluptates officiis assumenda beatae cumque, modi dolores maxime accusantium dolorem ipsa sunt, qui repellendus saepe temporibus iure placeat? Sint!
                Officia beatae quae ab laboriosam nam aliquid dignissimos repellat velit eius reprehenderit laborum asperiores omnis, deserunt doloribus suscipit tempora sapiente voluptas eos repudiandae excepturi voluptate. Odio saepe voluptatum inventore provident.
                Aliquam inventore quidem aliquid quos, dolorem similique tenetur impedit facere magni iusto modi, velit earum. Voluptate pariatur eius, quasi alias placeat ipsum, earum quis nesciunt optio quo tempora itaque voluptatibus.
                Ipsum tempora quas quam aliquid adipisci nam facilis praesentium error molestias rerum fugiat, ab sit earum dolores omnis soluta fuga at reprehenderit! Beatae reprehenderit perspiciatis obcaecati! Et quidem vel officia.
                Ea sequi nulla iste eius quasi impedit assumenda eveniet recusandae eos explicabo quidem quis nemo libero itaque aperiam neque laborum voluptas ipsam, aut exercitationem dicta. Nulla quae dolor eius beatae!
                Ratione deserunt ut neque aut officia fugit voluptatem velit possimus necessitatibus blanditiis autem quos incidunt porro deleniti delectus, consequatur nostrum maiores asperiores ipsam? Odio quisquam pariatur quas enim recusandae exercitationem?
                Iste aut animi, quos nisi quod laboriosam blanditnditiis atque odio totam consequuntur! Sunt dolore voluptatum, dicta est doloribus non repellendus molestias ipsum unde, delectus fugiat libero accusamus? Rerum, atque molestias.
                Quisquam doloribus iusto temporibus quidem dignissimos adipisci blanditiis tempora. Ad porro fugiat totam? Soluta vero odio laudantium officia nesciunt maiores. Velit ipsum consequuntur expedita totam. Veniam ad harum dolorum eos.
                Consequuntur animi nulla, eos eaque numquam veritatis tempora autem. Itaque accusantium a omnis aliquid ut delectus veniam animi fugiat, quibusdam non qui natus dolorem veritatis suscipit aliquam quis nostrum incidunt.
                Facere dicta ullam recusandae cupiditate eligendi quis voluptates illum. Voluptatibus nulla, commodi explicabo quod sequi, ratione maiores vero perspiciatis fuga ut veritatis quaerat, ea voluptates nesciunt animi doloremque rerum a.
                Earum doloribus quas praesentium totam distinctio eligendi iste molestias ratione cum! Provident aliquam modi exercitationem, dolor unde nisi, eaque, assumenda reprehenderit omnis consequatur ea adipisci ipsa alias iste error autem.
                Aut, tempore corrupti dolore sit, provident commodi laudantium, porro explicabo alias eveniet asperiores labore ducimus iste similique sed vel a. Tempore ratione quod eveniet sed veritatis nemo doloribus distinctio. Nesciunt.
                Excepturi at quas eum numquam facere temporibus repudiandae cupiditate impedit tenetur, mollitia iste sint nulla quae atque! Quisquam, harum a omnis eligendi reprehenderit alias numquam repudiandae mollitia reiciendis aperiam dignissimos?
                Fuga magnam ipsum sit iusto iure impedit assumenda autem harum incidunt nisi, architecto inventore ipsam omnis porro quam ducimus nulla vel magni corporis adipisci. Natus harum omnis laudantium ipsam a!
                Pariatur quasi adipisci eos, sequi, fugit itaque est officia necessitatibus expedita nam tempore excepturi sint doloremque suscipit nihil amet nesciunt? Repellendus, explicabo saepe necessitatibus praesentium culpa laudantium totam nesciunt veritatis!
                Aut laborum iusto dicta, molestiae odio rem laudantium corporis dolorem, iure dolore accusantium maxime similique cumque expedita esse voluptatibus earum porro laboriosam adipisci unde, ab aperiam impedit? Quia, libero commodi?
                Ullam quos pariatur maiores modi hic libero optio ad aperiam? Dolores cumque voluptatem consequatur nemo. Debitis deserunt dolor fugit, nihil pariatur non velit ipsum aspernatur odio neque totam, aliquid error!
                Possimus est nesciunt placeat accusantium molestias! Culpa consequuntur magni provident voluptatem! Vero dicta iure a officia neque nam, quam corrupti laudantium dolores sit quas accusamus ex natus amet? Quo, adipisci.
                Ratione cupiditate dolorum perferendis accusamus veniam autem accusantium repudiandae earum mollitia eligendi ducimus ad quaerat soluta, quia numquam dicta dolore ut eius illo obcaecati qui! Officiis tempore ipsam temporibus vero.
                Tenetur blanditiis nobis rem possimus aspernatur fugit, dolores ipsa quasi officiis a perferendis dicta vel temporibus eveniet ullam atque dolorem explicabo commodi quisquam? Unde est quidem nostrum in, quos vero!
                Dolore fuga pariatur, tenetur, dicta nulla ipsa mollitia aliquid sequi dolores repellendus fugit minima ducimus incidunt. Perferendis consequuntur temporibus sint, debitis tempore ipsum? Reiciendis atque quis eaque natus magni reprehenderit.
                Voluptatibus commodi nobis enim aspernatur, omnis laudantium nihil laborum rerum corrupti perferendis autem sit quod sed fugit! Dignissimos ullam quisquam, animi labore eos ipsa sunt dolores temporibus nihil illo vel.
                Eos voluptates reprehenderit ad qui eum corporis iste inventore amet, magnam, recusandae numquam ex natus, veritatis et debitis facilis dicta laborum doloremque. Delectus at ducimus dolore repellendus suscipit sapiente aliquid!</p>

                <p>You've scrolled this far: {scrollDepth}</p>

        </div>
    );
};