import React, { useState }  from 'react';


export default function useStateHook1() {
 
    const [cartItems, setCartItems] = useState([]);

    var cartItems
    
    return (
        <div>
        <p>Items in your cart: {cartItems.toString()}</p>
        <button 
            onClick={() => {
                setCartItems([...cartItems, 'apple'])
            }}
        >
            Add to cart
        </button>            
        </div>
       
        

    );
};