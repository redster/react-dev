import React from 'react';

import ChildPage from './ChildPage';

export default function(){
    return (
        <div>
            <ChildPage age="35" name="Alex"/>
            <ChildPage age="40" name="John"/>
            <ChildPage age="20" name="Tiffany"/>
            <ChildPage age="18" name="Anna"/>           
        </div>
    );
};