import React from 'react';

export default function(props){
    return(
        <div>
            <p>Имя: {props.name}</p>
            <p>Возвраст: {props.age}</p>        
        </div>        
    );
};